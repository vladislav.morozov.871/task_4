import com.github.javafaker.Faker;
import com.opencsv.CSVWriter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Main
{

    public static void main(String[] args) throws IOException
    {
        int recordsNum;
        String region;

        if (Check.isArgsCorrect(args))
        {
            recordsNum = Integer.parseInt(args[0]);
            region = args[1];

            Generator.generateFakersInCSV(region, recordsNum);
        }
    }
}

